/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2013 All Rights Reserved.
 */
package com.custom.app.service.task;

/**
 * 
 * @author wb-tianzd
 * @version $Id: Task.java, v 0.1 2013-10-18 上午10:39:45 wb-tianzd Exp $
 */
public class Task {

    public void work() {
        System.out.println("Quartz的任务调度！");
    }

}
